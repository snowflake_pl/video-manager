#include <gtest/gtest.h>

#include <directory_crawler.h>

namespace fs = boost::filesystem;

namespace consts {
const fs::path wrong_dir("/some/wrong/directory");
const fs::path testing_root("/home/sniegu/creator/ViedoManager/testing/testing_data/");
const fs::path empy_dir(testing_root/fs::path("empty_dir"));

const fs::path dir1(testing_root/fs::path("dir1"));
const directory_entries one_file{
    fs::directory_entry{dir1/fs::path("file.txt")}
};

const fs::path dir2(testing_root/fs::path("dir2"));
const directory_entries multiple_files{
    fs::directory_entry{dir2/fs::path("file1.txt")},
    fs::directory_entry{dir2/fs::path("file2.txt")},
    fs::directory_entry{dir2/fs::path("file3.txt")},
    fs::directory_entry{dir2/fs::path("file4.txt")}
};

const fs::path dir3(testing_root/fs::path("dir3"));
const directory_entries recursive_files{
    fs::directory_entry{dir3/fs::path("file1.txt")},
    fs::directory_entry{dir3/fs::path("file2.txt")},
    fs::directory_entry{dir3/fs::path("dir1")},
    fs::directory_entry{dir3/fs::path("dir2")},
    fs::directory_entry{dir3/fs::path("dir2/file1.txt")},
    fs::directory_entry{dir3/fs::path("dir2/file2.txt")}
};
} // namespace consts



class directory_crawler_test : public ::testing::Test
{
protected:
    directory_crawler sut;
};

TEST_F(directory_crawler_test, given_non_existing_directory_returns_empty_set)
{
    auto entries = sut.crawl(consts::wrong_dir);
    EXPECT_EQ(0, entries.size());
    EXPECT_TRUE(entries.empty());
}


TEST_F(directory_crawler_test, given_empty_directory_lists_no_files)
{
    auto entries = sut.crawl(consts::empy_dir);
    EXPECT_EQ(0, entries.size());
    EXPECT_TRUE(entries.empty());
}

TEST_F(directory_crawler_test, given_directory_with_one_file_list_one_file)
{
    auto entries = sut.crawl(consts::dir1);
    EXPECT_EQ(1, entries.size());
    EXPECT_EQ(consts::one_file, entries);
}

TEST_F(directory_crawler_test, given_directory_with_multiple_files_lists_them)
{
    auto entries = sut.crawl(consts::dir2);
    EXPECT_EQ(4, entries.size());
    EXPECT_EQ(consts::multiple_files, entries);
}

TEST_F(directory_crawler_test, recursive_crawling)
{
    auto entries = sut.crawl(consts::dir3);
    EXPECT_EQ(6, entries.size());
    EXPECT_EQ(consts::recursive_files, entries);
}
