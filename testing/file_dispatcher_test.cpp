#include <gtest/gtest.h>

#include <file_segregator.h>

namespace fs = boost::filesystem;
namespace consts {
const segregated_entries empty_sets;
const directory_entries empty_dirset;
const fs::directory_entry video_file_entr("/some/path/file.avi");
const fs::directory_entry subtitle_entr("/some/path/file.txt");
const segregated_entries video_only_seg{
    {EFile_type::Video, {video_file_entr}}
};
const segregated_entries seg_vid_subt{
    {EFile_type::Video, {video_file_entr}},
    {EFile_type::Subtitle, {subtitle_entr}}
};
} // namespace consts

class file_dispatcher_test : public ::testing::Test
{

protected:
    file_segregator sut;
};

TEST_F(file_dispatcher_test, given_empty_set_returns_empty_sets)
{
    EXPECT_EQ(consts::empty_sets, sut.segregate(consts::empty_dirset));
}

TEST_F(file_dispatcher_test, given_only_movie_returns_non_empty_video_set)
{
    EXPECT_EQ(consts::video_only_seg, sut.segregate({consts::video_file_entr}));
}

TEST_F(file_dispatcher_test, given_movie_and_subtitle_returns_two_sets)
{
    EXPECT_EQ(consts::seg_vid_subt, sut.segregate({consts::video_file_entr,
                                                   consts::subtitle_entr}));
}

