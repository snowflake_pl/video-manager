#include <gtest/gtest.h>
#include <path_creator.h>
#include <optional_compare_wrapper.h>
#include <file_info.h>

namespace fs = boost::filesystem;

namespace consts {
namespace dt = data_types;
using data_types::file_info;
static const std::string dummy_title("Dummy title");
static const file_info empty_info{};
static const dt::tvshow_info tv_show(1,21);
static const file_info title_only{dt::e_video_movie, dummy_title};
static const file_info title_and_year{dt::e_video_movie, dummy_title, 2015};
static const file_info no_title{dt::e_video_movie, boost::none, 2015};
static const file_info title_and_reso{dt::e_video_movie, dummy_title, boost::none, 720};
static const file_info title_year_reso{dt::e_video_movie, dummy_title, 2015, 720};
static const file_info series_title{dt::e_video_tvshow, dummy_title};
static const file_info full_series_info{dt::e_video_tvshow, dummy_title, 2015, 720, tv_show};
static const fs::path base("/some/base/dir");
static const fs::path movies("Filmy");
static const fs::path series("Seriale");
static const fs::path title_path(base / movies / fs::path("Dummy title") /
                                 fs::path("Dummy title.avi"));
static const fs::path title_year_path(base / movies / fs::path("Dummy title (2015)") /
                                      fs::path("Dummy title (2015).avi"));
static const fs::path title_reso_path(base / movies / fs::path("Dummy title [720p]") /
                                      fs::path("Dummy title [720p].avi"));
static const fs::path title_year_reso_path(base / movies / fs::path("Dummy title (2015) [720p]") /
                                           fs::path("Dummy title (2015) [720p].avi"));
static const fs::path series_title_path(base / series / fs::path("Dummy title") /
                                        fs::path("Dummy title.avi"));
static const fs::path series_title_path_with_eps(base / series /
                                                 fs::path("Dummy title (2015) [720p]") /
                                                 fs::path("Season 1/Dummy title S01E21.avi"));
} // namespace consts

class path_creator_test : public ::testing::Test, public optional_compare_wrapper
{
public:
    path_creator creator{consts::base, consts::movies, consts::series, consts::series,
                         consts::movies};
};

TEST_F(path_creator_test, given_empty_info_struct_returns_boost_none)
{
    ASSERT_EQ(boost::none, creator.create_path(consts::empty_info));
}

TEST_F(path_creator_test, given_no_title_returns_boost_none)
{
    ASSERT_EQ(boost::none, creator.create_path(consts::no_title));
}

TEST_F(path_creator_test, given_movie_title_returns_movie_title)
{
    ASSERT_EQ(O(consts::title_path), creator.create_path(consts::title_only));
}

TEST_F(path_creator_test, given_movie_title_and_year_returns_title_and_year_in_round_brackets)
{
    ASSERT_EQ(O(consts::title_year_path), creator.create_path(consts::title_and_year));
}

TEST_F(path_creator_test, given_movie_title_and_reso_returns_title_and_reso_in_square_brackets)
{
    ASSERT_EQ(O(consts::title_reso_path), creator.create_path(consts::title_and_reso));
}

TEST_F(path_creator_test, given_title_year_reso_returns_title_year_reso_in_proper_brackets)
{
    ASSERT_EQ(O(consts::title_year_reso_path), creator.create_path(consts::title_year_reso));
}

TEST_F(path_creator_test, given_tv_show_without_tv_show_info_returns_boost_none)
{
    ASSERT_EQ(boost::none, creator.create_path(consts::series_title));
}

TEST_F(path_creator_test, given_series_title_season_episode_returns_path_with_title_season_episode)
{
    ASSERT_EQ(O(consts::series_title_path_with_eps), creator.create_path(consts::full_series_info));
}
