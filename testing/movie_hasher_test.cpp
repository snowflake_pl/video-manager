#include <gtest/gtest.h>

#include <movie_hasher.h>

using boost::filesystem::path;
namespace consts {
const path incorrect_path("/non/existing/directory/and_file.avi");
const path breakdance_file("breakdance.avi");
const path dummy_rar("dummy.bin");

//taken from OpenSubtitles.org site with hash algorithm implementation
const std::string breakdance_cs_str("8e245d9679d31e12");
const std::string dummy_cs_str("61f7751fc2a72bfb");
}


class movie_hasher_test : public ::testing::Test
{

};

TEST_F(movie_hasher_test, given_wrong_path_returns_zero)
{
    movie_hasher sut(consts::incorrect_path);
    EXPECT_EQ("", sut());
}

TEST_F(movie_hasher_test, given_breakdance_avi_returns_correct_checksum)
{
    movie_hasher sut(consts::breakdance_file);
    EXPECT_EQ(consts::breakdance_cs_str, sut());
}

TEST_F(movie_hasher_test, DISABLED_given_dummy_rar_file_returns_correct_checksum)
{
    //test is too havy for normal work - tested and runs ok
    movie_hasher sut(consts::dummy_rar);
    EXPECT_EQ(consts::dummy_cs_str, sut());
}
