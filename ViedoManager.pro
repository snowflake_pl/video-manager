TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += src
INCLUDEPATH += testing

SOURCES += main.cpp \
    src/path_creator.cpp \
    testing/path_creator_test.cpp \
    src/file_info.cpp \
    testing/filesystem_operations_test.cpp \
    src/rpc_universal_value.cpp \
    src/movie_hasher.cpp \
    testing/movie_hasher_test.cpp \
    testing/direcotry_crawler_test.cpp \
    src/directory_crawler.cpp \
    testing/file_dispatcher_test.cpp \
    src/filesystem_operation.cpp \
    src/file_info_creator.cpp \
    src/videofile_descriptior.cpp \
    src/application_logic.cpp \
    src/file_segregator.cpp \
    async_operations.cpp \
    rpc_client.cpp \
    base64.cpp \
    gzip.cpp \
    md5.cpp

include(deployment.pri)
qtcAddDeployment()

LIBS += -lgmock \
        -lpthread \
        -lboost_system \
        -lboost_filesystem \
        -lxmlrpc++ \
        -lxmlrpc \
        -lxmlrpc_xmlparse \
        -lxmlrpc_xmltok \
        -lxmlrpc_util \
        -lxmlrpc_client \
        -lxmlrpc_client++ \
        -lboost_iostreams \
        -lz

QMAKE_CXXFLAGS += -std=c++1y
QMAKE_CXXFLAGS += -Werror

HEADERS += \
    src/path_creator.h \
    src/file_info.h \
    src/boost_optional_printer.h \
    src/optional_compare_wrapper.h \
    src/abstract_filesystem_operation.h \
    src/rpc_universal_value.h \
    src/movie_hasher.h \
    src/directory_crawler.h \
    src/enumerations.h \
    src/filesystem_operation.h \
    src/file_info_creator.h \
    src/videofile_descriptior.h \
    src/application_logic.h \
    src/application_parameters.h \
    src/typedefs.h \
    src/file_segregator.h \
    abstract_async_operation.h \
    async_operations.h \
    rpc_client.h \
    base64.h \
    gzip.h \
    md5.h

