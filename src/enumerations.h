#ifndef ENUMERATIONS_H
#define ENUMERATIONS_H

#include <stdint.h>

enum class EFile_type : uint_fast8_t
{
    Video,
    Subtitle,
    Directory,
    Other
};

enum class rpc_call : uint_fast8_t
{
	check_movie_hash,
	check_sub_hash,
	download_subtitles,
	login,
	logout,
	search_subtitles,
	upload_subtitles
};


#endif // ENUMERATIONS_H

