#ifndef OPTIONAL_COMPARE_WRAPPER_H
#define OPTIONAL_COMPARE_WRAPPER_H

#include <boost/optional.hpp>
#include <boost_optional_printer.h>
class optional_compare_wrapper
{
protected:
    template <typename T>
    boost::optional<T> O(const T& val)
    {
        return boost::optional<T>(val);
    }
};

#endif // OPTIONAL_COMPARE_WRAPPER_H

