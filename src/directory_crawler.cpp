#include "directory_crawler.h"

namespace consts {
const boost::filesystem::recursive_directory_iterator  end_iter;
} // namespace consts
directory_crawler::directory_crawler()
{

}

directory_crawler::~directory_crawler()
{

}

directory_entries directory_crawler::crawl(const boost::filesystem::path& path)
{
    namespace fs = boost::filesystem;
    using rec_iter = fs::recursive_directory_iterator;

    if (!fs::exists(path) || !fs::is_directory(path)) {
        return directory_entries{};
    }

    directory_entries result;
    for(rec_iter dir_iter(path) ; dir_iter != consts::end_iter ; ++dir_iter) {
        result.insert(*dir_iter);
    }
    return std::move(result);
}

