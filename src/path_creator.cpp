#include "path_creator.h"

#include <string>
#include <sstream>

#include <boost/format.hpp>

#include <file_info.h>

namespace fs = boost::filesystem;

namespace consts {
static const data_types::file_info empty_info{};
static const std::string res_bracket_op{" ["};
static const std::string res_bracket_cl{"p]"};
static const std::string year_bracket_op{" ("};
static const std::string year_bracket_cl{")"};
} // namespace consts

path_creator::path_creator(const boost::filesystem::path& base_dir,
                           const boost::filesystem::path& movie_dir,
                           const boost::filesystem::path& series_dir,
                           const boost::filesystem::path& anime_dir,
                           const boost::filesystem::path& others_dir)
    : base_dir{base_dir}
    , movie_dir{movie_dir}
    , series_dir{series_dir}
    , anime_dir(anime_dir)
    , others_dir(others_dir)
    , series_name_formatter{"%|1$1| S%|2$02|E%|3$02|%|4$1|"} //creates Title S01E01.ext
    , movie_name_formatter("%|1$1|%|2$1|")                   //creates Title.ext
{}

boost::optional<fs::path> path_creator::create_path(const data_types::file_info& info)
{
	if (info == consts::empty_info || !info.title
	        || (info.type == data_types::e_video_tvshow && !info.tv_show_info)) {
		return boost::none;
	}

	fs::path path{create_root_path_for_entry(info)};

	if (info.type == data_types::e_video_movie) {
		if (info.year) {
			path = add_year_information(path, info);
		}
		if (info.resolution) {
			path = add_resolution_information(path, info);
		}
		path /= movie_filename(path, info);
	} else if (info.type == data_types::e_video_tvshow) {
		path /= extract_series_part(info);
	} else {
		return boost::none;
	}
	return path;
}

fs::path path_creator::create_root_path_for_entry(const data_types::file_info& info)
{
	return video_type_root(info) / fs::path{info.title.get()};
}

fs::path path_creator::video_type_root(const::data_types::file_info& info)
{
	fs::path subdir{};
	switch (info.type) {
	case data_types::e_video_movie:
		subdir = movie_dir;
		break;
	case data_types::e_video_tvshow:
		subdir = series_dir;
		break;
	case data_types::e_video_anime:
		subdir = anime_dir;
		break;
	case data_types::e_video_other:
		subdir = others_dir;
		break;
	}
	return base_dir / subdir;
}

fs::path path_creator::add_resolution_information(const fs::path& path,
                                                  const data_types::file_info& info)
{
	return append_brackets_and_number(path, consts::res_bracket_op, *info.resolution,
	                                  consts::res_bracket_cl);
}

fs::path path_creator::add_year_information(const fs::path& path,
                                            const data_types::file_info& info)
{
	return append_brackets_and_number(path, consts::year_bracket_op, *info.year,
	                                  consts::year_bracket_cl);
}

boost::filesystem::path path_creator::append_brackets_and_number(const fs::path& path,
                                                                 const std::string& opening_bracket,
                                                                 size_t number,
                                                                 const std::string& closing_bracket)
{
	std::string file_name = path.filename().string();
	file_name += opening_bracket + std::to_string(number) + closing_bracket;

	return path.parent_path() / fs::path{file_name};
}

fs::path path_creator::extract_series_part(const data_types::file_info& info)
{
	std::string title = *info.title;
	std::string extension = info.extension;
	size_t season = info.tv_show_info->season;
	size_t episode = info.tv_show_info->episode;
	fs::path season_dir{(boost::format{"Season %1%"} %season).str()};
	std::string filename = (series_name_formatter %title %season %episode %extension).str();

	return season_dir / fs::path{filename};
}

boost::filesystem::path path_creator::movie_filename(const fs::path& path,
                                                     const data_types::file_info& info)
{
	return fs::path{(movie_name_formatter %path.filename().string() %info.extension).str()};
}
