#ifndef RPC_UNIVERSAL_VALUE_H
#define RPC_UNIVERSAL_VALUE_H

#include <boost/variant.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <map>
namespace xmlrpc_c {
class value; //forward declaration of value type
}

/*! \brief rpc_universal_value is defined as a variant that can
 *         take string, int, double, bool,
 *         and collections of itself, namely a vector and a map
 *
 *
 * Credit to:
 * Gerd Riesselmann
 * gerd@gerd-riesselmann.net
 */
typedef boost::make_recursive_variant<
    std::string,
    int,
    bool,
    double,
    std::map< std::string, boost::recursive_variant_ >,
    std::vector< boost::recursive_variant_ >
>::type rpc_universal_value;


/*! \brief Key and value pairs form a struct
 */
typedef std::map< std::string, rpc_universal_value > rpc_universal_struct;

/*! \brief A list forms an array
 */
typedef std::vector< rpc_universal_value > rpc_universal_vector;

rpc_universal_value convert(const xmlrpc_c::value& val);

rpc_universal_vector make_rpc_vector(const xmlrpc_c::value& val);

rpc_universal_struct make_rpc_struct(const xmlrpc_c::value& val);

std::ostream& operator<<(std::ostream& strm, const rpc_universal_value& val);


#endif // RPC_UNIVERSAL_VALUE_H
