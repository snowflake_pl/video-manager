#include <file_info.h>

#include <iostream>

namespace {
inline std::string extract_series_name(const std::string& title)
{
	constexpr char quotemark('\"');

	size_t start = title.find(quotemark);
	size_t end = title.rfind(quotemark);

	if (start == std::string::npos || end == std::string::npos) {
		return title;
	}
	return title.substr(start + 1, end - start - 1);
}
} // namespace unnamed

namespace data_types {

file_info::file_info(e_video_type video_type,
                     boost::optional<std::string> title,
                     boost::optional<size_t> year,
                     boost::optional<size_t> resolution,
                     boost::optional<tvshow_info> tv_show_info,
                     std::string extension)
    : type(video_type)
    , title(title)
    , year(year)
    , resolution(resolution)
    , tv_show_info(tv_show_info)
    , extension(extension)
{
	if (this->title.is_initialized() && type == e_video_type::e_video_tvshow) {
		this->title = extract_series_name(*title);
	}
}

bool file_info::operator==(const file_info & other) const
{
    return type == other.type && title == other.title && year == other.year
            && resolution == other.resolution && tv_show_info == other.tv_show_info;
}




} // namespace data_types
