#include "filesystem_operation.h"
#include <iostream>

#include <boost/filesystem/operations.hpp>

// Move file section
move_file_operation::move_file_operation(const fs::path& from, const fs::path& to)
    : from(from)
    , to(to)
{}

bool move_file_operation::execute() const
{
	if (!fs::exists(from)) {
		return false;
	}



	boost::system::error_code e;
	fs::create_directories(to.parent_path(), e);

	if (e && !fs::is_directory(to.parent_path())) {
		std::cout << "Could not create root directory for " << to.string() << std::endl;
		return false;
	}

	fs::rename(from, to, e);

	if (e) {
		std::cout <<"Error from move: " << e << std::endl << e.message() << std::endl;
		return false;
	}
	return true;
}

std::string move_file_operation::summary() const
{
	return "File " + from.string() + " will be moved to " + to.string();
}
// end of Move file section

// Copy file section
copy_file_operation::copy_file_operation(const fs::path& from, const fs::path& to)
    : from(from)
    , to(to)
{}

bool copy_file_operation::execute() const
{
	if (!fs::exists(from)) {
		return false;
	}

	boost::system::error_code e;
	fs::copy(from, to, e);
	return !e;
}

std::string copy_file_operation::summary() const
{
	return "File " + from.string() + " will be copied to " + to.string();
}
// end of Copy file section

// Delete file section
delete_file_operation::delete_file_operation(const boost::filesystem::path& path)
    : path(path)
{}

bool delete_file_operation::execute() const
{
	if (!fs::exists(path)) {
		return false;
	}

	boost::system::error_code e;
	fs::remove(path, e);
	return !e;
}

std::string delete_file_operation::summary() const
{
	return "File " + path.string() + " will be deleted";
}
// end of Delete file section


create_directory_operation::create_directory_operation(const boost::filesystem::path& path)
    : path(path)
{}

bool create_directory_operation::execute() const
{
	boost::system::error_code e;
	return (fs::create_directories(path, e) && !e) || fs::is_directory(path);
}

std::string create_directory_operation::summary() const
{
	return "Directory " + path.string() + " will be crated";
}
