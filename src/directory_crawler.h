#ifndef DIRECTORY_CRAWLER_H
#define DIRECTORY_CRAWLER_H

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <set>

using directory_entries = std::set<boost::filesystem::directory_entry>;

class directory_crawler
{
public:
    directory_crawler();
    ~directory_crawler();

    directory_entries crawl(const boost::filesystem::path& path);
};

#endif // DIRECTORY_CRAWLER_H
