#ifndef FILE_DISPATCHER_H
#define FILE_DISPATCHER_H

#include <map>
#include <set>
#include <string>
#include <boost/filesystem/operations.hpp>

#include <typedefs.h>

class file_segregator
{
public:
    file_segregator() = default;
    ~file_segregator() = default;

    segregated_entries segregate(const directory_entries& entries);
};

#endif // FILE_DISPATCHER_H
