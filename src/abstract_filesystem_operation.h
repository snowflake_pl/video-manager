#ifndef ABSTRACT_FILESYSTEM_OPERATION_H
#define ABSTRACT_FILESYSTEM_OPERATION_H

#include <string>

class abstract_filesystem_operation
{
public:
    virtual ~abstract_filesystem_operation() {}
    virtual bool execute() const = 0;
    virtual std::string summary() const = 0;
};


#endif // ABSTRACT_FILESYSTEM_OPERATION_H

