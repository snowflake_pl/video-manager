#include "file_info_creator.h"

namespace consts {
static const std::string movie_kind_movie{"movie"};
static const std::string movie_kind_episode{"episode"};
static const std::string movie_kind{"MovieKind"};
static const std::string movie_name{"MovieName"};
static const std::string movie_year{"MovieYear"};
static const std::string episode{"SeriesEpisode"};
static const std::string season{"SeriesSeason"};
static const std::string IMDB_ID{"MovieImdbID"};
} //namespace consts


namespace {
data_types::e_video_type get_file_type(const rpc_universal_struct& map)
{
	if (map.count(consts::movie_kind) == 0) {
		return data_types::e_video_movie;
	}

	auto type_string = boost::get<std::string>(map.at(consts::movie_kind));
	if (type_string == consts::movie_kind_movie) {
		 return data_types::e_video_movie;
	} else if (type_string == consts::movie_kind_episode) {
		return data_types::e_video_tvshow;
	} else {
		return data_types::e_video_other;
	}
}

boost::optional<std::string> get_title(const rpc_universal_struct& map)
{
	if (map.count(consts::movie_name) == 0) {
		return boost::none;
	}

	return boost::get<std::string>(map.at(consts::movie_name));
}

boost::optional<size_t> get_year(const rpc_universal_struct& map)
{
	if (map.count(consts::movie_year) == 0) {
		return boost::none;
	}
	const std::string year_string = boost::get<std::string>(map.at(consts::movie_year));
	return std::stoul(year_string);
}

boost::optional<size_t> get_resolution(const rpc_universal_value& /*value*/)
{
	return boost::none;
}

inline size_t get_season(const rpc_universal_struct& map)
{
	if (map.count(consts::season) == 0) {
		return 0;
	}
	const std::string season_string = boost::get<std::string>(map.at(consts::season));
	return std::stoul(season_string);
}

inline size_t get_episode(const rpc_universal_struct& map)
{
	if (map.count(consts::episode) == 0) {
		return 0;
	}
	const std::string episode_string = boost::get<std::string>(map.at(consts::episode));
	return std::stoul(episode_string);
}

boost::optional<data_types::tvshow_info> get_tv_show_info(const rpc_universal_struct& map)
{
	if (get_file_type(map) != data_types::e_video_tvshow) {
		return boost::none;
	}

	return data_types::tvshow_info(get_season(map), get_episode(map));

}

std::string get_extension(const fs::path& path)
{
	return path.extension().string();
}

} //namespace unnamed

file_info_creator::file_info_creator()
{}

boost::optional<data_types::file_info> file_info_creator::create_file_info(
        const rpc_universal_value& val,
        const fs::path& path) const
{
	if (val.which() != 4) {
		return boost::none;
	}

	const rpc_universal_struct& map = boost::get<rpc_universal_struct>(val);


	return data_types::file_info(
		get_file_type(map),
		get_title(map),
		get_year(map),
		get_resolution(map),
		get_tv_show_info(map),
		get_extension(path)
	);
}

