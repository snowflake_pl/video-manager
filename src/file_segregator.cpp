#include "file_segregator.h"
#include <boost/filesystem/fstream.hpp>

namespace fs = boost::filesystem;

namespace consts {
const fs::path video_extensions_location("/home/sniegu/creator/ViedoManager/resources/video_extensions.txt");
const fs::path subtitle_extensions_location("/home/sniegu/creator/ViedoManager/resources/subtitle_extensions.txt");

std::set<fs::path> load_from_file(const fs::path& location)
{
    fs::ifstream file(location);

    if (!file.is_open()) {
        return {};
    }

    std::set<fs::path> result;
    std::string line;
    while (!file.eof()) {
        file >> line;
        result.insert(fs::path(line));
    }
    return std::move(result);
}
std::set<fs::path> load_video_extensions()
{
    return load_from_file(consts::video_extensions_location);
}

std::set<fs::path> load_subtitle_extensions()
{
    return load_from_file(consts::subtitle_extensions_location);
}

const std::set<fs::path> video_extensions = load_video_extensions();
const std::set<fs::path> subtitle_extensions = load_subtitle_extensions();
} // namespace consts

bool is_directory(const fs::directory_entry& entry)
{
    return entry.status().type() == fs::file_type::directory_file;
}

bool is_video(const fs::directory_entry& entry)
{
    return consts::video_extensions.count(entry.path().extension()) > 0;
}

bool is_subtitle(const fs::directory_entry& entry)
{
    return consts::subtitle_extensions.count(entry.path().extension()) > 0;
}

EFile_type type_of(const boost::filesystem::directory_entry& entry)
{
    if (is_directory(entry)) {
        return EFile_type::Directory;
    }
    if (is_video(entry)) {
        return EFile_type::Video;
    }
    if (is_subtitle(entry)) {
        return EFile_type::Subtitle;
    }
    return EFile_type::Other;
}

segregated_entries file_segregator::segregate(const directory_entries& entries)
{
    if (entries.empty()) {
        return {};
    }

    segregated_entries result;
    for (const auto& entry : entries) {
        result[type_of(entry)].insert(entry);
    }
    return std::move(result);
}

