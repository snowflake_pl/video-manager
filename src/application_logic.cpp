#include <algorithm>
#include <stdexcept>

#include "application_logic.h"

#include <directory_crawler.h>
#include <file_segregator.h>
#include <movie_hasher.h>
#include <file_info_creator.h>
#include <path_creator.h>
#include <filesystem_operation.h>
#include <async_operations.h>

#include <XmlRpcCpp.h>
#include <xmlrpc-c/client_simple.hpp>
#include <rpc_universal_value.h>

namespace consts {
constexpr size_t hash_limit = 200;
}

application_logic::application_logic(application_parameters params)
    : params(std::move(params))
{}

void application_logic::run()
{
	if (!OS_API_client.is_good()) {
		std::cout <<"Could not login to OS API" <<std::endl;
		return;
	}
	std::cout <<"print_application_parameters_summary()" << std::endl;
	print_application_parameters_summary();
	std::cout <<"get_directory_entries_from_original_library();" <<std::endl;
	get_directory_entries_from_original_library();
	std::cout <<"segregate_entries_by_file_type();" <<std::endl;
	if (!segregate_entries_by_file_type()) {
		std::cout <<"No video files to work with..." << std::endl;
		return;
	}
	std::cout <<"create_hashmap_with_movie_files();" <<std::endl;
	create_hashmap_with_movie_files();
	std::cout <<"generate_xml_rpc_hash_array();" <<std::endl;
	generate_xml_rpc_hash_array();
	std::cout <<"split_hash_array_into_queries();" <<std::endl;
	split_hash_array_into_queries();
	std::cout <<"execute_xml_rpc_queries();" <<std::endl;
	execute_xml_rpc_queries();
	std::cout <<"parse_xml_rpc_outputs();" <<std::endl;
	parse_xml_rpc_outputs();
	std::cout <<"generate_new_paths_for_parsed_files();" <<std::endl;
	generate_new_paths_for_parsed_files();
	std::cout <<"create_appropriate_filesystem_operations();" <<std::endl;
	create_appropriate_filesystem_operations();
	std::cout <<"show_summary();" <<std::endl;
	show_summary();
	std::cout <<"wait_for_accpeptance()" << std::endl;
	if (!get_user_acceptance()) {
		std::cout <<"User did not accept execution..." <<std::endl;
		return;
	} else {
		std::cout <<"Proceding with operations..." << std::endl;
		std::cout <<"execute_filesystem_actions();" <<std::endl;
		execute_filesystem_actions();
	}
}

void application_logic::print_application_parameters_summary()
{
	std::cout <<"Params summary: " <<std::endl;
	std::cout <<"Original library path: " << params.original_library << std::endl;
	std::cout <<"New library root: " << params.new_library_root << std::endl;
	std::cout <<"Movies subdirectory: " << params.movies_directory << std::endl;
	std::cout <<"Series subdirectory: " << params.series_directory<< std::endl;
	std::cout <<"Anime subdirectory: " << params.anime_directory << std::endl;
	std::cout <<"Others subdirectory: " << params.others_directory << std::endl;
}

void application_logic::get_directory_entries_from_original_library()
{
	raw_entries = directory_crawler().crawl(params.original_library);
}

bool application_logic::segregate_entries_by_file_type()
{
	seg_entries = file_segregator().segregate(raw_entries);
	raw_entries.clear();
	return seg_entries.count(EFile_type::Video);
}

void application_logic::create_hashmap_with_movie_files()
{
	for (const auto& movie : seg_entries.at(EFile_type::Video)) {
		fs::path path = movie.path();
		std::string hash = movie_hasher(path)();
		original_paths.insert({hash, path});
	}
}

void application_logic::generate_xml_rpc_hash_array()
{
	hash_array.reserve(original_paths.size());
	for (const auto& hash_entry : original_paths) {
		hash_array.push_back(xmlrpc_c::value_string(hash_entry.first));
	}
}

void application_logic::split_hash_array_into_queries()
{
	size_t query_count = 1 + hash_array.size() / consts::hash_limit;
	queries.resize(query_count);

	size_t iteration = 0;
	for (auto& query : queries) {
		auto beg = hash_array.begin() + (iteration++) * consts::hash_limit;
		auto end = (iteration == queries.size()) ? hash_array.end() : beg + consts::hash_limit;
		query.resize(end-beg);
		std::move(beg, end, query.begin());
	}
	hash_array.clear();
}

void application_logic::execute_xml_rpc_queries()
{
	query_results.reserve(queries.size());
	for (const auto& query : queries) {
		query_results.push_back(execute_query(query));
	}
}

xmlrpc_c::value application_logic::execute_query(const xml_hash_vector& query)
{
	return OS_API_client.execute(rpc_call::check_movie_hash, query);
}

void application_logic::parse_xml_rpc_outputs()
{
	for (const auto& result : query_results) {
		parse_xml_output(result);
	}
}

void application_logic::parse_xml_output(const xmlrpc_c::value& hash)
{
	rpc_universal_struct res = boost::get<rpc_universal_struct>(convert(hash));
	if (res.count("data") == 0) {
		throw std::runtime_error("Invalid xml hash - it has no DATA");
	}

	file_info_creator creator;
	std::string result_hash;
	rpc_universal_value xml_hash;
	auto untie = std::tie(result_hash, xml_hash);

	auto data = boost::get<rpc_universal_struct>(res.at("data"));
	for (const auto& val : data) {
		untie = val;

		if (original_paths.count(result_hash) == 0) {
			unprocessed_hashes.push_back(result_hash);
			continue;
		}

		const fs::path& path = original_paths.at(result_hash);
		auto file_info = creator.create_file_info(xml_hash, path);
		if (file_info.is_initialized()) {
			file_info_map[result_hash] = file_info.get();
		} else {
			unprocessed_hashes.push_back(result_hash);
		}
	}
}

void application_logic::generate_new_paths_for_parsed_files()
{
	std::string hash;
	data_types::file_info info;
	auto untie = std::tie(hash, info);
	path_creator creator(params.new_library_root, params.movies_directory,
	                     params.series_directory, params.anime_directory,
	                     params.original_library);
	for (const auto& entry : file_info_map) {
		untie = entry;
		boost::optional<fs::path> new_path = creator.create_path(info);

		if (new_path.is_initialized()) {
			new_paths[hash] = new_path.get();
		} else {
			unprocessed_hashes.push_back(hash);
		}
	}
}

void application_logic::create_appropriate_filesystem_operations()
{
	fs::path old_path, new_path;
	std::string hash;
	auto untie = std::tie(hash, new_path);

	filesystem_operations.reserve(5 + new_paths.size());

	create_root_library_directory_creation();

	for (const auto& entry : new_paths) {
		untie = entry;
		old_path = original_paths.at(hash);
		create_filesystem_operations(old_path, new_path, hash);
	}
}

void application_logic::create_root_library_directory_creation()
{
	filesystem_operations.push_back(
	            std::make_unique<create_directory_operation>(params.new_library_root));
	filesystem_operations.push_back(
	            std::make_unique<create_directory_operation>(params.new_library_root / params.anime_directory));
	filesystem_operations.push_back(
	            std::make_unique<create_directory_operation>(params.new_library_root / params.movies_directory));
	filesystem_operations.push_back(
	            std::make_unique<create_directory_operation>(params.new_library_root / params.series_directory));
	filesystem_operations.push_back(
	            std::make_unique<create_directory_operation>(params.new_library_root / params.others_directory));
}

void application_logic::create_filesystem_operations(const fs::path& old_path,
                                                     const fs::path& new_path,
                                                     const std::string& hash)
{
	add_movie_move_operation(new_path, old_path);
	size_t size = boost::filesystem::file_size(old_path);
	auto subtitle_path = get_subtitle_path(old_path);
	if (subtitle_path) {
		add_subtitle_move_operation(subtitle_path.get(), new_path);
		add_subtitle_upload_operation(hash, subtitle_path.get());
	} else {
		add_to_subtitle_download_list(hash, size, new_path);
	}
}

void application_logic::add_movie_move_operation(const fs::path& new_path, const fs::path& old_path)
{
	filesystem_operations.push_back(
	            std::make_unique<move_file_operation>(old_path, new_path));
}

boost::optional<fs::path> application_logic::get_subtitle_path(const fs::path& file_path)
{
	directory_entries subtitles = get_subtitles_from_directory(file_path.parent_path());

	const auto has_matching_filename = [&file_path](const directory_entries::value_type& subtitle)
	                             { return subtitle.path().stem() == file_path.stem();};

	auto subtitle_iter = std::find_if(subtitles.begin(), subtitles.end(), has_matching_filename);

	return (subtitle_iter == subtitles.end()) ? boost::optional<fs::path>(boost::none)
	                                          : subtitle_iter->path();
}

 directory_entries application_logic::get_subtitles_from_directory(const fs::path& directory)
{
	auto entries = directory_crawler().crawl(directory);
	auto seg_entries = file_segregator().segregate(entries);
	return std::move(seg_entries[EFile_type::Subtitle]);
}

void application_logic::add_subtitle_move_operation(const fs::path& sub_path,
                                                    const fs::path& new_file_path)
{
	fs::path new_path = new_file_path;
	new_path.replace_extension(sub_path.extension());
	filesystem_operations.push_back(
	            std::make_unique<move_file_operation>(sub_path, new_path));
}

void application_logic::add_to_subtitle_download_list(const std::string& hash,
                                                      const size_t size,
                                                      const fs::path& path)
{
	sub_download_list.push_back(std::make_unique<subtitle_download_opeartion>(hash, size, path));
}

void application_logic::add_subtitle_upload_operation(const std::string& hash, const fs::path& sub_path)
{
	sub_download_list.push_back(std::make_unique<subtitle_upload_operation>(hash, 0, sub_path));
}

void application_logic::show_summary()
{
	for (const auto& op : filesystem_operations) {
		std::cout << op->summary() << std::endl;
	}

	for (const auto& down : sub_download_list) {
		std::cout << down->summary() << std::endl;
	}

	std::cout <<"Not processed: " <<std::endl;
	for (const auto& hash : unprocessed_hashes) {
		std::cout <<"Hash: " <<hash << (original_paths.count(hash) ? original_paths.at(hash) : "no source file with this hash") <<std::endl;
	}
}

bool application_logic::get_user_acceptance()
{
	const std::string accept = "yes";
	std::cout <<"Do you accept? yes / no" <<std::endl;
	std::string answer;
	std::cin >> answer;
	return answer == accept;
}

void application_logic::execute_filesystem_actions()
{
	size_t success = 0;
	size_t failure = 0;

	spawn_subtitle_download();

	for (const auto& op : filesystem_operations) {
		if (op->execute()) {
			std::cout <<"Executed " << ++success << " out of " << filesystem_operations.size() << " operations" <<std::endl;
			++success;
		} else {
			std::cout <<"Failed " << ++failure << " out of " << filesystem_operations.size() << " operations" <<std::endl;
			++success;
		}
	}
	wait_for_subtitle_download_finish();
}

void application_logic::spawn_subtitle_download()
{
	for (const auto& entry : sub_download_list) {
		entry->run();
	}
}

void application_logic::wait_for_subtitle_download_finish()
{
	size_t success = 0;
	size_t failure = 0;
	for (const auto& entry : sub_download_list) {
		if (entry->wait()) {
			std::cout <<"Executed " << ++success <<" out of " << sub_download_list.size() <<" downloads" <<std::endl;
		} else {
			std::cout <<"Failed to download " << ++failure <<" out of " <<sub_download_list.size() <<std::endl;
		}
	}
}
