#ifndef FILE_INFO_CREATOR_H
#define FILE_INFO_CREATOR_H

#include <boost/filesystem/path.hpp>

#include <file_info.h>
#include <rpc_universal_value.h>

namespace fs=boost::filesystem;

class file_info_creator
{
public:
	file_info_creator();

	boost::optional<data_types::file_info> create_file_info(const rpc_universal_value& val,
	                                       const fs::path& path) const;

};

#endif // FILE_INFO_CREATOR_H
