#ifndef MOVIE_HASHER_H
#define MOVIE_HASHER_H

#include <stdint-gcc.h>
#include<boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>


/**
 * @brief The movie_hasher class
 *
 * Class for calculating hash values for movies. Hashes are used by openSubtitles.
 */
class movie_hasher
{
public:
    /**
     * @brief movie_hasher - construcotr of movie_hasher class taking path to file
     * @param path - path to file to calculate hash for
     */
    movie_hasher(const boost::filesystem::path& path);

    /**
     *@brief movie hasher - destructor
     */
    ~movie_hasher();

    /**
     * @brief operator () - calculates hash for movie given in constructor
     * @return hash of movie given in constructor
     */
    std::string operator()();

private:
    uint64_t calculate_hash();
    uint64_t calculate_filesize();
    uint64_t calculate_checksum_at_current_position();
    bool read_chunk(uint64_t * tmp);
    void move_file_to_end();

private:
    boost::filesystem::fstream file;
    uint64_t file_size;
};

#endif // MOVIE_HASHER_H
