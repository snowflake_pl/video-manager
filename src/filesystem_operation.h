#ifndef FILESYSTEM_OPERATION_H
#define FILESYSTEM_OPERATION_H

#include <abstract_filesystem_operation.h>
#include <boost/filesystem/path.hpp>

namespace fs=boost::filesystem;

/// \brief Class representing filesystem operation of moving a file
class move_file_operation : public abstract_filesystem_operation
{
public:
	move_file_operation(const fs::path& from, const fs::path& to);
	bool execute() const override;
	std::string summary() const override;
private:
	fs::path from;
	fs::path to;
};


/// \brief Class representing filesystem operation of copying a file
class copy_file_operation : public abstract_filesystem_operation
{
public:
	copy_file_operation(const fs::path& from, const fs::path& to);
	bool execute() const override;
	std::string summary() const override;
private:
	fs::path from;
	fs::path to;
};


/// \brief Class representing filesystem operation of deleting a file
class delete_file_operation : public abstract_filesystem_operation
{
public:
	delete_file_operation(const fs::path& path);
	bool execute() const override;
	std::string summary() const override;
private:
	fs::path path;
};

class create_directory_operation : public abstract_filesystem_operation
{
public:
	create_directory_operation(const fs::path& path);
	bool execute() const override;
	std::string summary() const override;
private:
	fs::path path;
};

#endif // FILESYSTEM_OPERATION_H
