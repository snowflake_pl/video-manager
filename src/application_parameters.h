#ifndef APPLICATION_PARAMETERS_H
#define APPLICATION_PARAMETERS_H

#include <boost/filesystem/path.hpp>

namespace fs = boost::filesystem;

struct application_parameters
{
	fs::path original_library;
	fs::path new_library_root;
	fs::path movies_directory;
	fs::path series_directory;
	fs::path anime_directory;
	fs::path others_directory;
};



#endif // APPLICATION_PARAMETERS_H

