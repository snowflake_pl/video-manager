#ifndef PATH_CREATOR_H
#define PATH_CREATOR_H

#include <boost/optional.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

namespace data_types {
struct file_info;
} // namespace data_types

class path_creator
{
public:
    path_creator(const boost::filesystem::path& base_dir,
                 const boost::filesystem::path& movie_dir,
                 const boost::filesystem::path& series_dir,
	             const boost::filesystem::path & anime_dir,
	             const boost::filesystem::path & others_dir);
    boost::optional<boost::filesystem::path> create_path(const data_types::file_info& info);
private:
    boost::filesystem::path create_root_path_for_entry(const data_types::file_info & info);
    boost::filesystem::path video_type_root(const::data_types::file_info & info);
    boost::filesystem::path add_year_information(const boost::filesystem::path& path,
                                                     const data_types::file_info& info);
    boost::filesystem::path add_resolution_information(const boost::filesystem::path& path,
                                                           const data_types::file_info& info);
    boost::filesystem::path append_brackets_and_number(const boost::filesystem::path& path,
                                                       const std::string& opening_bracket,
                                                       size_t number,
                                                       const std::string& closing_bracket);
    void add_series_related_information(boost::filesystem::path& path,
                                        const data_types::file_info& info);
    boost::filesystem::path extract_series_part(const data_types::file_info & info);
    boost::filesystem::path movie_filename(const boost::filesystem::path& path,
                                           const data_types::file_info& info);
private:
    boost::filesystem::path base_dir;
    boost::filesystem::path movie_dir;
    boost::filesystem::path series_dir;
    boost::filesystem::path anime_dir;
    boost::filesystem::path others_dir;
    boost::format series_name_formatter;
    boost::format movie_name_formatter;

};

#endif // PATH_CREATOR_H
