#ifndef BOOST_OPTIONAL_PRINTER_H
#define BOOST_OPTIONAL_PRINTER_H

#include <boost/optional.hpp>
namespace boost {
template <typename T> inline
::std::ostream& operator<<(::std::ostream& os, const optional<T>& value)
{
    return os << (value ? *value : "boost::none");
}
} // namespace boost

#endif // BOOST_OPTIONAL_PRINTER_H

