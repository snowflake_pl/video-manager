#ifndef FILE_INFO_H
#define FILE_INFO_H

#include <string>
#include <boost/optional.hpp>

namespace data_types {

enum e_video_type
{
    e_video_movie,
    e_video_tvshow,
    e_video_anime,
    e_video_other
};

struct tvshow_info
{

    tvshow_info(const tvshow_info& other)
        : tvshow_info(other.season, other.episode)
    {}

    tvshow_info(size_t season, size_t episode)
        : season(season)
        , episode(episode)
    {}

    inline bool operator==(const tvshow_info& other) const
    {
        return episode == other.episode && season == other.season;
    }

    size_t season;
    size_t episode;
};

struct file_info
{
    file_info(e_video_type video_type = e_video_movie,
              boost::optional<std::string> title = boost::none,
              boost::optional<size_t> year = boost::none,
              boost::optional<size_t> resolution = boost::none,
              boost::optional<tvshow_info> tv_show_info = boost::none,
              std::string extension = ".avi");

    bool operator==(const file_info& other) const;

    e_video_type type;
    boost::optional<std::string> title;
    boost::optional<size_t> year;
    boost::optional<size_t> resolution;
    boost::optional<tvshow_info> tv_show_info;
    std::string extension;
};


} // namespace data_types
#endif // FILE_INFO_H
