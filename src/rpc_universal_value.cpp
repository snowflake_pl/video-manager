#include "rpc_universal_value.h"

#include <xmlrpc-c/base.hpp>

std::string make_string_from(const xmlrpc_c::value& val)
{
    auto bytestring = xmlrpc_c::value_bytestring(val).cvalue();
    return std::string(bytestring.begin(), bytestring.end());
}

rpc_universal_vector make_rpc_vector(const xmlrpc_c::value& val)
{
    rpc_universal_vector vec;
    for (const auto& ref : xmlrpc_c::value_array(val).cvalue()) {
        vec.push_back(convert(ref));
    }
    return vec;
}

rpc_universal_struct make_rpc_struct(const xmlrpc_c::value& val)
{
    rpc_universal_struct vec;
    for (const auto& ref : xmlrpc_c::value_struct(val).cvalue()) {
        vec.insert(std::make_pair(ref.first, convert(ref.second)));
    }
    return vec;
}

rpc_universal_value convert(const xmlrpc_c::value& val)
{
    switch (val.type()) {
    case xmlrpc_c::value::TYPE_C_PTR:
    case xmlrpc_c::value::TYPE_NIL:
    case xmlrpc_c::value::TYPE_I8:
    case xmlrpc_c::value::TYPE_DEAD:
        return {};
    case xmlrpc_c::value::TYPE_INT:
        return rpc_universal_value(xmlrpc_c::value_int(val).cvalue());
    case xmlrpc_c::value::TYPE_BOOLEAN:
        return rpc_universal_value(xmlrpc_c::value_boolean(val).cvalue());
    case xmlrpc_c::value::TYPE_DOUBLE:
        return rpc_universal_value(xmlrpc_c::value_double(val).cvalue());
    case xmlrpc_c::value::TYPE_DATETIME:
        return rpc_universal_value(xmlrpc_c::value_datetime(val).iso8601Value());
    case xmlrpc_c::value::TYPE_STRING:
        return rpc_universal_value(xmlrpc_c::value_string(val).cvalue());
    case xmlrpc_c::value::TYPE_BYTESTRING:
        return rpc_universal_value(make_string_from(val));
    case xmlrpc_c::value::TYPE_ARRAY:
        return rpc_universal_value(make_rpc_vector(val));
    case xmlrpc_c::value::TYPE_STRUCT:
        return rpc_universal_value(make_rpc_struct(val));
    }

    // Unrecognized data type
    throw std::invalid_argument("Unknown data type detected");
}

std::ostream& print_map(std::ostream& strm, const rpc_universal_struct& map)
{
	strm << std::endl;
    for (const auto& ref : map) {
        strm << "case 7: " << ref.first << " : " << ref.second <<std::endl;
    }
    return strm;
}

std::ostream& print_vec(std::ostream& strm, const rpc_universal_vector& vec)
{
    for (const auto& ref : vec) {
        strm << ref << std::endl;
    }
    return strm;
}

std::ostream& operator<<(std::ostream& strm, const rpc_universal_value& val)
{
    switch(val.which()) {
    case 0:
        return strm << std::endl << "case 0: " << boost::get<std::string>(val);
    case 1:
        return strm << std::endl << "case 1: " << boost::get<int>(val);
    case 2:
        return strm << std::endl << "case 2: " << boost::get<bool>(val);
    case 3:
        return strm << std::endl << "case 3: " << boost::get<double>(val);
    case 4:
		strm << std::endl << "case 4: ";
        return print_map(strm, boost::get<rpc_universal_struct>(val));
    case 5:
		strm << std::endl << "case 5: ";
        return print_vec(strm, boost::get<rpc_universal_vector>(val));
    }
    return strm;
}


