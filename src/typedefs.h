#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <set>
#include <map>

#include <boost/filesystem.hpp>

#include <enumerations.h>

using directory_entries = std::set<boost::filesystem::directory_entry>;
using segregated_entries = std::map<EFile_type, directory_entries>;

#endif // TYPEDEFS_H

