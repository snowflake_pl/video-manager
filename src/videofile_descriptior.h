#ifndef VIDEOFILE_DESCRIPTIOR_H
#define VIDEOFILE_DESCRIPTIOR_H

#include <memory>

#include <boost/filesystem/path.hpp>

#include <file_info.h>

namespace fs = boost::filesystem;

class videofile_descriptior
{
public:
	videofile_descriptior();



private:
	fs::path original_path;
	std::unique_ptr<data_types::file_info> file_information;

};

#endif // VIDEOFILE_DESCRIPTIOR_H
