#ifndef APPLICATION_LOGIC_H
#define APPLICATION_LOGIC_H

#include <map>
#include <string>
#include <memory>
#include <stdint.h>

#include <application_parameters.h>
#include <typedefs.h>
#include <typedefs.h>
#include <XmlRpcCpp.h>
#include <xmlrpc-c/client_simple.hpp>
#include <rpc_universal_value.h>
#include <rpc_client.h>
#include <file_info.h>
#include <abstract_filesystem_operation.h>
#include <abstract_async_operation.h>

using hashmap = std::map<std::string, fs::path>;
using xml_hash_vector = std::vector<xmlrpc_c::value>;
using xml_queries = std::vector<xml_hash_vector>;
using file_info_map_type = std::map<std::string, data_types::file_info>;
using filesystem_operation_handle = std::unique_ptr<abstract_filesystem_operation>;
using async_operation_handle = std::unique_ptr<abstract_async_operation>;

class application_logic
{
public:
	application_logic(application_parameters params);

	void run();
private:
	application_parameters params;
	directory_entries raw_entries;
	segregated_entries seg_entries;
	hashmap original_paths;
	hashmap new_paths;
	xml_hash_vector hash_array;
	xml_queries queries;
	std::string OS_login_token;
	rpc_client OS_API_client;
	xml_hash_vector query_results;
	file_info_map_type file_info_map;
	std::vector<std::string> unprocessed_hashes;
	std::vector<filesystem_operation_handle> filesystem_operations;
	std::vector<async_operation_handle> sub_download_list;
private:
	void print_application_parameters_summary();
	void get_directory_entries_from_original_library();
	bool segregate_entries_by_file_type();
	void create_hashmap_with_movie_files();
	void generate_xml_rpc_hash_array();
	void split_hash_array_into_queries();
	void execute_xml_rpc_queries();
	xmlrpc_c::value execute_query(const xml_hash_vector& query);
	void parse_xml_rpc_outputs();
	void parse_xml_output(const xmlrpc_c::value& hash);
	void generate_new_paths_for_parsed_files();
	void create_appropriate_filesystem_operations();
	void create_root_library_directory_creation();
	void create_filesystem_operations(const fs::path& old_path,
	                                  const fs::path& new_path, const std::string& hash);
	void show_summary();
	bool get_user_acceptance();
	void execute_filesystem_actions();
	void add_movie_move_operation(const fs::path& new_path, const fs::path& old_path);
	boost::optional<fs::path> get_subtitle_path(const fs::path& file_path);
	void add_subtitle_move_operation(const fs::path& sub_path, const fs::path& new_file_path);
	void add_to_subtitle_download_list(const std::string& hash, const size_t size, const fs::path& path);
	directory_entries get_subtitles_from_directory(const fs::path& directory);
	void spawn_subtitle_download();
	void wait_for_subtitle_download_finish();
	void unpack_and_store_downloaded_subtitles();
	void add_subtitle_upload_operation(const std::string& hash, const fs::path& sub_path);
};

#endif // APPLICATION_LOGIC_H
