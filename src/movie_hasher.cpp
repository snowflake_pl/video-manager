#include "movie_hasher.h"
#include <sstream>

namespace {
namespace consts {
static constexpr uint32_t checksum_length = 65536;
static constexpr uint64_t number_of_read_required = checksum_length/sizeof(uint64_t);
static const std::string empty_hash;
}

template <typename T>
inline T MAX(T x, T y)
{
    return x > y ? x : y;
}

template< typename T >
std::string hexify(T hash)
{
    std::stringstream stream;
    stream << std::hex << hash;
    return stream.str();
}
}

movie_hasher::movie_hasher(const boost::filesystem::path & path) :
    file(path, std::ios::in | std::ios::binary | std::ios::ate),
    file_size(0)
{
}

movie_hasher::~movie_hasher()
{
}

std::string movie_hasher::operator()()
{
    return file.is_open() ? hexify(calculate_hash()) : consts::empty_hash;
}

uint64_t movie_hasher::calculate_hash()
{
    auto hash = calculate_filesize();
    hash += calculate_checksum_at_current_position();
    move_file_to_end();
    hash += calculate_checksum_at_current_position();
    return hash;
}

uint64_t movie_hasher::calculate_filesize()
{
    file.seekg(0, std::ios::end);
    file_size = uint64_t(file.tellg());
    file.seekg(0, std::ios::beg);
    return file_size;
}

uint64_t movie_hasher::calculate_checksum_at_current_position()
{
    auto tmp = uint64_t(0);
    auto hash = tmp;
    auto read_number = tmp;

    for(;read_number <  consts::number_of_read_required && read_chunk(&tmp); ++read_number) {
        hash += tmp;
    }
    return hash;
}

bool movie_hasher::read_chunk(uint64_t * tmp)
{
    return file.read(reinterpret_cast<char*>(tmp), sizeof(*tmp));
}

void movie_hasher::move_file_to_end()
{
    auto end_position = MAX(uint64_t(0), file_size - consts::checksum_length);
    file.seekg(end_position, std::ios::beg);
}



